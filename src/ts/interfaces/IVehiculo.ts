export interface IVehiculo {
    id_vehiculo?: number,
    cilindros: number,
    puertas: number,
    asientos: number,
    idmarca: number,
    idlinea: number,
    idsublinea: number
    nombre_marca?: string,
    nombre_linea?: string,
    nombre_sublinea?: string
}