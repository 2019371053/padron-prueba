interface ISublinea {
    id?: number,
    nombre: string,
    idlinea: number,
    id_linea?: any,
    id_marca?: any
}

export {
    ISublinea
}