import {ILinea} from "./interfaces/ILinea";

const urlGet: string = "http://172.31.101.187:8089/ords/ws_padron/marca/linea";
const urlPost: string = "http://172.31.101.187:8089/ords/ws_padron/marca/linea";
const urlPut: string = "http://172.31.101.187:8089/ords/ws_padron/marca/linea";
const urlDelete: string = "http://172.31.101.187:8089/ords/ws_padron/marca/linea";

export async function get() {
    return new Promise((resolve, reject) => {
        const headers = {
            "Content-Type": "application/json"
        };

        fetch(urlGet, {
            method: "GET",
            headers: headers,
            mode: "cors"
        })
            .then(resp => resp.json())
            .then((data) => {

                resolve(data.items)
            })
            .catch((error) => {
                reject(error)
            })
    })
}

export async function create(data: ILinea) {
    return new Promise((resolve, reject) => {
        const headers = {
            "Content-Type": "application/json"
        };
        const body = JSON.stringify(data);

        fetch(urlPost, {
            method: "POST",
            headers: headers,
            body: body,
            mode: "cors"
        })
            .then(resp => resp)
            .then((data) => {
                resolve(data)
            })
            .catch((error) => {
                reject(error)
            })
    })
}

export async function update(data: ILinea) {
    return new Promise((resolve, reject) => {
        const headers = {
            "Content-Type": "application/json"
        };

        

        const body = JSON.stringify(data);

        console.log(data)

        fetch(urlPut, {
            method: "PUT",
            headers: headers,
            body: body,
            mode: "cors"
        })
            .then(resp => resp)
            .then((data) => {
                resolve(data)
            })
            .catch((error) => {
                reject(error)
            })
    })
}

export async function del(id: number) {
    return new Promise((resolve, reject) => {
        const headers = {
            "Content-Type": "application/json"
        };


        const data = {
            id :id
        }

        const body = JSON.stringify(data);


        fetch(urlDelete, {
            method: "DELETE",
            headers: headers,
            body: body,
            mode: "cors"
        })
            .then(resp => resp)
            .then((data) => {
                resolve(data)
            })
            .catch((error) => {
                reject(error)
            })
    })
}