import { APIURL } from './api_config';
import { ISublinea } from '../interfaces/ISublinea';

const urlGet: string = `${APIURL}/sublinea`;
const urlPost: string = `${APIURL}/sublinea`;
const urlPut: string = `${APIURL}/sublinea`;
const urlDelete: string = `${APIURL}/sublinea`;

export async function get() {
    return new Promise((resolve, reject) => {
        const headers = {
            "Content-Type": "application/json"
        };

        fetch(urlGet, {
            method: "GET",
            headers: headers,
            mode: "cors"
        })
            .then(resp => resp.json())
            .then((data) => {
                resolve(data.items)
            })
            .catch((error) => {
                reject(error)
            })
    })
}

export async function create(data: ISublinea) {
    return new Promise((resolve, reject) => {
        const headers = {
            "Content-Type": "application/json"
        };
        const body = JSON.stringify(data);

        fetch(urlPost, {
            method: "POST",
            headers: headers,
            body: body,
            mode: "cors"
        })
            .then(resp => {
                return resp
            })
            .then((data) => {
                resolve(data)
            })
            .catch((error) => {
                reject(error)
            })
    })
}

export async function update(data: ISublinea) {
    return new Promise((resolve, reject) => {
        const headers = {
            "Content-Type": "application/json"
        };
        const body = JSON.stringify(data);

        fetch(urlPut, {
            method: "PUT",
            headers: headers,
            body: body,
            mode: "cors"
        })
            .then(resp => resp)
            .then((data) => {
                resolve(data)
            })
            .catch((error) => {
                reject(error)
            })
    })
}

export async function del(id: number) {
    return new Promise((resolve, reject) => {
        const headers = {
            "Content-Type": "application/json"
        };

        let data = {
            id: id
        }
        const body = JSON.stringify(data);

        fetch(urlDelete, {
            method: "DELETE",
            headers: headers,
            body: body,
            mode: "cors"
        })
            .then(resp => resp)
            .then((data) => {
                resolve(data)
            })
            .catch((error) => {
                reject(error)
            })
    })
}