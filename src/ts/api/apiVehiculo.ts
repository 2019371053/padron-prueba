import { IVehiculo } from "../interfaces/IVehiculo";
import { APIURL } from "./api_config";

export async function getVehiculosApi() {
    return new Promise((resolve, reject) => {
        const headers = {
            "Content-Type": "application/json"
        }

        fetch(`${APIURL}/vehiculo`, {
            method: "GET",
            headers: headers,
            mode: "cors"
        })
            .then(resp => resp.json())
            .then((data) => {
                resolve(data)
            })
            .catch((error) => {
                reject(error);
            })
    })
}

export async function postVehiculoApi(vehiculo: IVehiculo) {
    return new Promise((resolve, reject) => {
        const headers = {
            "Content-Type": "application/json"
        }

        fetch(`${APIURL}/vehiculo`, {
            method: "POST",
            headers: headers,
            mode: "cors",
            body: JSON.stringify(vehiculo)
        })
            .then(resp => resp)
            .then((data) => {
                resolve(data)
            })
            .catch((error) => {
                reject(error);
            })
    })
}

export async function putVehiculoApi(vehiculo: IVehiculo) {
    return new Promise((resolve, reject) => {
        const headers = {
            "Content-Type": "application/json"
        }

        fetch(`${APIURL}/vehiculo`, {
            method: "PUT",
            headers: headers,
            mode: "cors",
            body: JSON.stringify(vehiculo)
        })
            .then(resp => resp)
            .then((data) => {
                resolve(data)
            })
            .catch((error) => {
                reject(error);
            })
    })
}

export async function deleteVehiculoApi(id: number) {
    return new Promise((resolve, reject) => {
        const headers = {
            "Content-Type": "application/json"
        }

        fetch(`${APIURL}/vehiculo`, {
            method: "DELETE",
            headers: headers,
            mode: "cors",
            body: JSON.stringify({
                id: id
            })
        })
            .then(resp => resp)
            .then((data) => {
                resolve(data)
            })
            .catch((error) => {
                reject(error);
            })
    })
}