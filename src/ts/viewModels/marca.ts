import * as AccUtils from "../accUtils";
import * as ko from "knockout";


import { ojDialog } from "ojs/ojdialog";
import { ojButton } from "ojs/ojbutton";
import { ojTable } from "ojs/ojtable";
import { KeySetImpl } from "ojs/ojkeyset";


import "ojs/ojdialog";
import "ojs/ojtable";
import "ojs/ojinputtext";
import "ojs/ojinputnumber";
import "ojs/ojknockout";
import ArrayDataProvider = require("ojs/ojarraydataprovider");

//interfaz de marca

import IMarca from "../interfaces/IMarca";

import { get, create,update,del} from "../api";

class MarcaViewModel {

  marca: ko.ObservableArray<IMarca>;
  marcaDataProvider: ArrayDataProvider<ko.ObservableArray, null>


  //Variable para crear 
  nombreMarca: ko.Observable<string>;

  //variable para editar
  idMarcaEditar: ko.Observable<number>;
  nombreMarcaEditar: ko.Observable<string>;


  constructor() {
    //Tabla
    this.marca = ko.observableArray([]);
    this.marcaDataProvider = new ArrayDataProvider(this.marca, {
      keyAttributes: 'id'
    })

    //Formulario crear
    this.nombreMarca = ko.observable("");

    //Formulario editar
    this.nombreMarcaEditar = ko.observable("");
    this.idMarcaEditar = ko.observable(0);


    //Traer datos
    this.getMarcaApi();

  }

  //Funcio para crear marca
  guardarMarca = (event: ojButton.ojAction) => {
    const nuevaMarca: IMarca = {
      nombre: this.nombreMarca()
    }
    create(nuevaMarca).then((response: IMarca) => {

      //Cerrar modal 
      (document.getElementById("crear-marca") as ojDialog).close();

      console.log(nuevaMarca)

    //Recargar los datos
      this.getMarcaApi();

    }).catch(error =>{
      console.error(error);
    });
  
  }
  
  //Funcion editar marca
  public editarMarca = (event: ojButton.ojAction) => {
    if (this.nombreMarcaEditar().trim() !== '') {
      const nuevaMarca: IMarca = {
        id: this.idMarcaEditar(),
        nombre: this.nombreMarcaEditar(),
      }

      update(nuevaMarca).then((response: IMarca) => {        
        (document.getElementById("editar-marca") as ojDialog).close();

        this.getMarcaApi();

      }).catch(error => {
        console.error(error);
      });
    }
  }

  //Seleccionar datos de la tabla

  public enSeleccionMarca = (event: ojTable.selectedChanged<IMarca["id"], IMarca>) => {
    let selectionText: string;

    const row = event.detail.value.row as KeySetImpl<number>;

    if (row.values().size > 0) {
      row.values().forEach((key) => {
        var selectedRow = this.marca().find(p => p.id === key);

        this.idMarcaEditar(key);
        this.nombreMarcaEditar(selectedRow.nombre);
      })
    }
  }


  //Funcion eliminar
  public eliminarMarca = (event: ojButton.ojAction) => {
    //Llamamos a la APi para crear producto
    del(this.idMarcaEditar()).then((response: IMarca) => {
      //Cerramos el modal y recargamos datos 
      (document.getElementById("eliminar-marca") as ojDialog).close();

      this.getMarcaApi();

    }).catch(error => {
      console.error(error);
    });
  }



  //Abrir y cerrar el modal para crear marca

  public openCrearMarca(event: ojButton.ojAction) {
    (document.getElementById("crear-marca") as ojDialog).open();
  }
  public closeCrearMarca(event: ojButton.ojAction) {
    (document.getElementById("crear-marca") as ojDialog).close();
  }

  //Abrir y cerrar el modal para editar marca

  public openEditarMarca(event: ojButton.ojAction) {
    (document.getElementById("editar-marca") as ojDialog).open();
  }
  public closeEditarMarca(event: ojButton.ojAction) {
    (document.getElementById("editar-marca") as ojDialog).close();
  }
   //Abrir y cerrar el modal para eliminar marca

   public openEliminarMarca(event: ojButton.ojAction) {
    (document.getElementById("eliminar-marca") as ojDialog).open();
  }
  public closeEliminarMarca(event: ojButton.ojAction) {
    (document.getElementById("eliminar-marca") as ojDialog).close();
  }

  //optener marcas
  public getMarcaApi = () => {
    get().then((response: Array<IMarca>) => {
      this.marca(response)

    }).catch((error) => {
      console.error(error);
    })
  }

}

export = MarcaViewModel;
