import * as AccUtils from "../accUtils";
import * as ko from "knockout";
import { ojTable } from "ojs/ojtable"
import { ojDialog } from 'ojs/ojdialog'
import { ojSelectSingle } from 'ojs/ojselectsingle'
import { IVehiculo } from "../interfaces/IVehiculo"
import ArrayDataProvider = require("ojs/ojarraydataprovider");
import { deleteVehiculoApi, getVehiculosApi, postVehiculoApi, putVehiculoApi } from "../api/apiVehiculo";
import { get as getSublineasApi } from "../api/apiSubLinea";
import { get as getLineasApi } from "../apiLineas";
import { get as getMarcasApi } from "../api";
import "ojs/ojtable";
import "ojs/ojknockout";
import "ojs/ojbutton";
import "ojs/ojdialog";
import "ojs/ojinputnumber";
import "ojs/ojselectsingle";
import { ISublinea } from "../interfaces/ISublinea";
import { KeySetImpl } from "@oracle/oraclejet/dist/types/ojkeyset";

interface ISelect { value: string, label: string };

class VehiculosViewModel {
    columnas: ojTable.Column<null, null>[];
    vehiculos: ko.ObservableArray<IVehiculo>;
    vehiculosDataProvider: ArrayDataProvider<ko.ObservableArray, any>;
    sublineas: ko.ObservableArray<ISelect>;
    sublineasDataProvider: ArrayDataProvider<ko.Observable, any>;
    lineas: ko.ObservableArray<ISelect>;
    lineasDataProvider: ArrayDataProvider<ko.Observable, any>;
    marcas: ko.ObservableArray<ISelect>;
    marcasDataProvider: ArrayDataProvider<ko.Observable, any>;

    //Crear
    cilindros: ko.Observable<number>;
    puertas: ko.Observable<number>;
    asientos: ko.Observable<number>;
    idmarca: ko.Observable<string>;
    idlinea: ko.Observable<string>;
    idsublinea: ko.Observable<string>;

    //Crear
    id_vehiculo: ko.Observable<number>;
    cilindrosEditar: ko.Observable<number>;
    puertasEditar: ko.Observable<number>;
    asientosEditar: ko.Observable<number>;
    idmarcaEditar: ko.Observable<string>;
    idlineaEditar: ko.Observable<string>;
    idsublineaEditar: ko.Observable<string>;

    constructor() {
        this.columnas = [
            {
                headerText: "ID",
                field: "id_vehiculo",
                headerClassName: "oj-sm-only-hide",
                className: "oj-sm-only-hide",
                resizable: "enabled",
                id: "ID-vehiculo"
            },
            {
                headerText: "Cilindros",
                field: "cilindros",
                headerClassName: "oj-sm-only-hide",
                className: "oj-sm-only-hide",
                resizable: "enabled",
                id: "ID-cilindros"
            },
            {
                headerText: "Puertas",
                field: "puertas",
                headerClassName: "oj-sm-only-hide",
                className: "oj-sm-only-hide",
                resizable: "enabled",
                id: "ID-puertas"
            },
            {
                headerText: "Asientos",
                field: "asientos",
                headerClassName: "oj-sm-only-hide",
                className: "oj-sm-only-hide",
                resizable: "enabled",
                id: "ID-asientos"
            },
            {
                headerText: "Marca",
                field: "nombre_marca",
                headerClassName: "oj-sm-only-hide",
                className: "oj-sm-only-hide",
                resizable: "enabled",
                id: "ID-marca"
            },
            {
                headerText: "Linea",
                field: "nombre_linea",
                headerClassName: "oj-sm-only-hide",
                className: "oj-sm-only-hide",
                resizable: "enabled",
                id: "ID-linea"
            },
            {
                headerText: "Sublinea",
                field: "nombre_sublinea",
                headerClassName: "oj-sm-only-hide",
                className: "oj-sm-only-hide",
                resizable: "enabled",
                id: "ID-sublinea"
            },
            {
                headerText: "Acciones",
                field: "acciones",
                headerClassName: "oj-sm-only-hide",
                className: "oj-sm-only-hide",
                resizable: "enabled",
                id: "ID-acciones",
                template: "celda_acciones_template"
            },
        ];

        this.vehiculos = ko.observableArray([]);
        this.vehiculosDataProvider = new ArrayDataProvider(this.vehiculos, {
            keyAttributes: "id_vehiculo"
        })

        this.marcas = ko.observableArray([]);
        this.marcasDataProvider = new ArrayDataProvider(this.marcas, {
            keyAttributes: "value"
        })

        this.lineas = ko.observableArray([]);
        this.lineasDataProvider = new ArrayDataProvider(this.lineas, {
            keyAttributes: "value"
        })

        this.sublineas = ko.observableArray([]);
        this.sublineasDataProvider = new ArrayDataProvider(this.sublineas, {
            keyAttributes: "value"
        })

        //Variables para crear
        this.cilindros = ko.observable(3);
        this.puertas = ko.observable(4);
        this.asientos = ko.observable(2);
        this.idmarca = ko.observable("");
        this.idlinea = ko.observable("");
        this.idsublinea = ko.observable("");

        this.id_vehiculo = ko.observable(0);
        this.cilindrosEditar = ko.observable(0);
        this.puertasEditar = ko.observable(0);
        this.asientosEditar = ko.observable(0);
        this.idmarcaEditar = ko.observable("");
        this.idlineaEditar = ko.observable("");
        this.idsublineaEditar = ko.observable("");

        this.getVehiculos();
        this.getMarcas();
    }

    public anadirVehiculo = () => {
        const vh: IVehiculo = {
            cilindros: this.cilindros(),
            asientos: this.asientos(),
            puertas: this.puertas(),
            idmarca: parseInt(this.idmarca()),
            idlinea: parseInt(this.idlinea()),
            idsublinea: parseInt(this.idsublinea())
        }

        postVehiculoApi(vh).then(() => {
            this.getVehiculos();
            this.closeAnadirModal();
        }).catch(err => {
            console.error(err);
        })
    }

    public editarVehiculo = () => {
        const vh: IVehiculo = {
            id_vehiculo: this.id_vehiculo(),
            cilindros: this.cilindrosEditar(),
            asientos: this.asientosEditar(),
            puertas: this.puertasEditar(),
            idmarca: parseInt(this.idmarcaEditar()),
            idlinea: parseInt(this.idlineaEditar()),
            idsublinea: parseInt(this.idsublineaEditar())
        }

        putVehiculoApi(vh).then(() => {
            this.getVehiculos();
            this.closeEditarModal();
        }).catch(err => {
            console.error(err);
        })
    }

    public eliminarVehiculo = () => {
        deleteVehiculoApi(this.id_vehiculo()).then(() => {
            this.getVehiculos();
            this.closeEliminarModal();
        }).catch(err => {
            console.error(err);
        })
    }

    public seleccionarCelda = (event: ojTable.selectedChanged<IVehiculo['id_vehiculo'], IVehiculo>) => {
        const row = event.detail.value.row as KeySetImpl<number>;

        if (row.values().size > 0) {
            row.values().forEach((key) => {
                var selectedRow = this.vehiculos().find(v => v.id_vehiculo === key);

                this.id_vehiculo(selectedRow.id_vehiculo);
                this.cilindrosEditar(selectedRow.cilindros);
                this.puertasEditar(selectedRow.puertas);
                this.asientosEditar(selectedRow.asientos);
                this.idmarcaEditar(selectedRow.idmarca.toString());
                this.idlineaEditar(selectedRow.idlinea.toString());
                this.idsublineaEditar(selectedRow.idsublinea.toString());
            })
        }
    }

    public getVehiculos() {
        getVehiculosApi().then((response: any) => {
            this.vehiculos(response.items as Array<IVehiculo>);
        }).catch(err => {
            console.error(err);
        })
    }

    public getMarcas() {
        getMarcasApi().then((response: Array<ISublinea>) => {
            let valuesSelect: ISelect[] = [];

            response.forEach(element => {
                valuesSelect.push({
                    value: element.id.toString(),
                    label: element.nombre
                })
            });

            this.marcas(valuesSelect);

        }).catch(err => {
            console.error(err);
        })
    }

    public getLineas = (event: ojSelectSingle.valueChanged<string, Record<string, string>>) => {
        let idMarca: number = parseInt(event.detail.value);

        getLineasApi().then((response: Array<ISublinea>) => {
            let valuesSelect: ISelect[] = [];

            response.forEach(element => {
                if (element.id_marca === idMarca) {
                    valuesSelect.push({
                        value: element.id.toString(),
                        label: element.nombre
                    })
                }
            });

            this.lineas(valuesSelect);

        }).catch(err => {
            console.error(err);
        })
    }

    public getSublineas = (event: ojSelectSingle.valueChanged<string, Record<string, string>>) => {
        let idLinea: number = parseInt(event.detail.value);

        getSublineasApi().then((response: Array<ISublinea>) => {
            let valuesSelect: ISelect[] = [];

            response.forEach(element => {
                if (element.id_linea === idLinea) {
                    valuesSelect.push({
                        value: element.id.toString(),
                        label: element.nombre
                    })
                }
            });

            this.sublineas(valuesSelect);

        }).catch(err => {
            console.error(err);
        })
    }

    public openAnadirModal() {
        (document.getElementById("modalAnadir") as ojDialog).open();
    }

    public closeAnadirModal() {
        (document.getElementById("modalAnadir") as ojDialog).close();
    }

    public openEditarModal() {
        (document.getElementById("modalEditar") as ojDialog).open();
    }

    public closeEditarModal() {
        (document.getElementById("modalEditar") as ojDialog).close();
    }

    public openEliminarModal() {
        (document.getElementById("modalEliminar") as ojDialog).open();
    }

    public closeEliminarModal() {
        (document.getElementById("modalEliminar") as ojDialog).close();
    }

    connected(): void {
        AccUtils.announce("Vehioculos page loaded.");
        document.title = "Vehículos";
        // implement further logic if needed
    }
}

export = VehiculosViewModel;