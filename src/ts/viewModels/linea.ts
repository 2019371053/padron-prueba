import * as ko from "knockout";
import ArrayDataProvider = require("ojs/ojarraydataprovider");
import { ojDialog } from "ojs/ojdialog";
import { ojButton } from "ojs/ojbutton";
import { ojTable } from "ojs/ojtable";
import { KeySetImpl } from "ojs/ojkeyset";

import "ojs/ojbutton";
import "ojs/ojdialog";
import "ojs/ojtable";
import "ojs/ojinputtext";
import "ojs/ojinputnumber";
import "ojs/ojknockout";
import "ojs/ojselectsingle";

import { ojSelectSingle } from 'ojs/ojselectsingle';

//Interfaces
import { ILinea } from "../interfaces/ILinea";
import  IMarca from "../interfaces/IMarca";
import { get, create, update, del } from "../apiLineas";
import { get as getM } from "../api";

//Class agregaMarca


class LineasViewModel {

  //Variables

  marcas : ko.ObservableArray<IMarca>;
  marcasDataProvider: ArrayDataProvider<ko.Observable, any>;

  lineas: ko.ObservableArray<ILinea>;
  lineasDataProvider: ArrayDataProvider<ko.ObservableArray, any>; 
  

  //Crear producto
  nombreLinea: ko.Observable<string>;
  idMarca: ko.Observable<number>;

  //Editar producto
  idLineaEditar: ko.Observable<number>;
  idMarcaEditar: ko.Observable<string>;
  nombreMarcaEditar: ko.Observable<string>;
  nombreLineaEditar: ko.Observable<string>;

  constructor() {
    //Inicializamos nuestras props 
    this.lineas= ko.observableArray([]);
    this.lineasDataProvider = new ArrayDataProvider(this.lineas, {
      //Atributo de identificador
      keyAttributes: 'id',
      //Order de la tabla
      //implicitSort: [{ attribute: 'id', direction: 'descending' }]
    })

    //Crear
    this.nombreLinea = ko.observable("");
    this.idMarca = ko.observable(0);

    //Editar
    this.nombreLineaEditar = ko.observable("");
    this.nombreMarcaEditar = ko.observable("");
    this.idMarcaEditar = ko.observable("");
    this.idLineaEditar = ko.observable(0);

  
    //Marcas
    this.marcas = ko.observableArray([]);
    this.marcasDataProvider = new ArrayDataProvider(this.marcas,{
      keyAttributes: "value",
    })

    //Llamamos la API
    this.getLineasApi();
    this.getMarcasApi();
  }


  
  //Función para guardar (crear) un producto
  public guardarProducto = (event: ojButton.ojAction) => {
    //Validaciones

    if (this.nombreLinea().trim() !== '') {
      //Creamos nuestro objeto para el body
      const Linea: ILinea = {
        nombre: this.nombreLinea(),
        idmarca: this.idMarca()
      }
      //Llamamos a la APi para crear producto
      create(Linea).then((response: ILinea) => {

        //Cerramos el modal y recargamos datos 
        (document.getElementById("crear-producto-dialog") as ojDialog).close();

        this.getLineasApi();

      }).catch(error => {
        console.error(error);
      });
    }
  }

  //Función para editar datos en servidor
  public editarProducto = (event: ojButton.ojAction) => {
    if (this.nombreLineaEditar().trim() !== '') {
      //Creamos nuestro objeto para el body
      const Linea: ILinea = {
        id: this.idLineaEditar(),
        nombre: this.nombreLineaEditar(),
        idmarca: this.idMarca(),
      }

      //Llamamos a la APi para crear producto
      update(Linea).then((response: ILinea) => {
        //Cerramos el modal y recargamos datos 
        
        (document.getElementById("editar-producto-dialog") as ojDialog).close();
  
        this.getLineasApi();

      }).catch(error => {
        console.error(error);
      });
    }
  }

  //Función para eliminar producto
  public eliminarProducto = (event: ojButton.ojAction) => {
    //Llamamos a la APi para crear producto
    del(this.idLineaEditar()).then((response: ILinea) => {
      //Cerramos el modal y recargamos datos 
      (document.getElementById("eliminar-producto-dialog") as ojDialog).close();
      this.getLineasApi();

    }).catch(error => {
      console.error(error);
    });
  }

  //Selección de datos en tabla
  public selectedChangedListener = (event: ojTable.selectedChanged<ILinea["id"], ILinea>) => {
    let selectionText: string;

    const row = event.detail.value.row as KeySetImpl<number>;

    if (row.values().size > 0) {
      row.values().forEach((key) => {
        var selectedRow = this.lineas().find(p => p.id === key);
        //asignamos ID seleccionado
        this.idLineaEditar(key);
        this.idMarcaEditar(selectedRow.id_marca.toString())

        console.log(this.idMarcaEditar()) 
        /* this.idMarcaEditar(selectedRow.id_marca.toString()); */
        this.nombreLineaEditar(selectedRow.nombre);
      })
    }
  };

  //Funciones para abrir y cerrar el modal para crear y editar
  public closeCrearProducto(event: ojButton.ojAction) {
    (document.getElementById("crear-producto-dialog") as ojDialog).close();
  }

  public openCrearProducto(event: ojButton.ojAction) {
    (document.getElementById("crear-producto-dialog") as ojDialog).open();
  }

  public openEditarProducto() {
    (document.getElementById("editar-producto-dialog") as ojDialog).open();
  }

  public closeEditarProducto(event: ojButton.ojAction) {
    (document.getElementById("editar-producto-dialog") as ojDialog).close();
  }

  public openEliminarProducto() {
    (document.getElementById("eliminar-producto-dialog") as ojDialog).open();
  }

  public closeEliminarProducto(event: ojButton.ojAction) {
    (document.getElementById("eliminar-producto-dialog") as ojDialog).close();
  }

  //Función para traer los datos del servicio
  public getLineasApi = () => {
    get().then((response: Array<ILinea>) => {
      this.lineas(response);
      console.log(response)
    }).catch(error => {
      console.error(error);
    })
  }

  public marcaSelect = ( event: ojSelectSingle.valueChanged<string, Record<string, string>> ) =>{
    this.idMarca(parseInt(event.detail.value));
   }



  //Funciones para obtener las marcas
  public getMarcasApi = () =>{
    getM().then((response: Array<IMarca>) => {
      let arreglo = []
      response.map((item, index)=>{
          arreglo.push({value:item.id.toString(), label:item.nombre})     
      })
      this.marcas(arreglo);
    }).catch((error) => {
      console.error(error);
    })
  }
}



export = LineasViewModel  ;
