import * as AccUtils from "../accUtils";
import * as ko from "knockout";
import ArrayDataProvider = require("ojs/ojarraydataprovider");
import { ISublinea } from '../interfaces/ISublinea';

import "ojs/ojtable";
import "ojs/ojknockout";
import "ojs/ojbutton";
import "ojs/ojdialog";
import "ojs/ojinputtext";
import "ojs/ojselectsingle";
import "ojs/ojlabel";

import { get as getLinea } from "../apiLineas";
import { get, create, update, del } from "../api/apiSubLinea";
import { get as getMarca } from "../api";


import { ojTable } from "ojs/ojtable";
import { KeySetImpl } from "@oracle/oraclejet/dist/types/ojkeyset";
import { ojButton } from "ojs/ojbutton";
import { ojDialog } from "ojs/ojdialog";
import { ojSelectSingle } from 'ojs/ojselectsingle';
import { ILinea } from "../interfaces/ILinea";
import IMarca from "../interfaces/IMarca";

class SubLineaViewModel {

  //columna
  columnas: ojTable.Column<null, null>[];

  //Declaración de variables
  marcas : ko.ObservableArray<IMarca>;
  marcasDataProvider: ArrayDataProvider<ko.Observable, any>;


  lineas: ko.ObservableArray<ILinea>;
  lineaDataProvider: ArrayDataProvider<ko.Observable, any>;

  sublineas: ko.ObservableArray<ISublinea>;
  sublineaDataProvider: ArrayDataProvider<ko.ObservableArray, any>;

  //formulacio crear
  nombreSubLinea: ko.Observable<string>;
  idlinea: ko.Observable<number>;


  //formulario editar
  idSelecion: ko.Observable<number>;
  nombreSubLineaeditar: ko.Observable<string>;
  idMarcaEditar: ko.Observable<number>;
  idLineaEditar: ko.Observable<number>;


  constructor() {
    this.columnas = [
      {
        headerText: "Sub linea Id", 
        field: "id",
        headerClassName: "oj-sm-only-hide",
        className: "oj-sm-only-hide",
        resizable: "enabled",
        id: "depId"
      },
      {
        headerText: "Marca", 
        field: "nombre_marca",
        resizable: "enabled",
        id: "depName"
      },
      {
        headerText: "Linea", 
        field: "nombre_linea",
        resizable: "enabled",
        id: "depName"
      },
      {
        headerText: "Sublinea", 
        field: "nombre",
        resizable: "enabled",
        id: "depName"
      },
      {
        headerText: "Acciones", 
        field: "actions",
        headerClassName: "oj-sm-only-hide",
        className: "oj-sm-only-hide",
        resizable: "enabled",
        template: "cellTemplate",
        id: "locId"
      }
];


    //crear
    this.nombreSubLinea = ko.observable("");
    this.idlinea = ko.observable(0);

    //editar
    this.nombreSubLineaeditar = ko.observable("");
    this.idMarcaEditar = ko.observable(0);
    this.idLineaEditar = ko.observable(0);
    this.idSelecion = ko.observable(0);

    //Iniciar
    this.sublineas = ko.observableArray([]);
    this.sublineaDataProvider = new ArrayDataProvider(this.sublineas, {
      keyAttributes: 'id',
      //implicitSort: [{attribute:'id', direction: 'descending'}]
    })
    this.lineas = ko.observableArray([]);
    this.lineaDataProvider = new ArrayDataProvider(this.lineas, {
      keyAttributes: "value",
    })

    this.marcas = ko.observableArray([]);
    this.marcasDataProvider = new ArrayDataProvider(this.marcas,{
      keyAttributes: "value",
    })

    this.getSubLineasApi();
    this.getMarcasApi();

  }

  public getMarcasApi = () =>{
    getMarca().then((response: Array<IMarca>) => {
      let arreglo = []
      response.map((item, index)=>{
        arreglo.push({value:item.id.toString(), label:item.nombre})
      })
      this.marcas(arreglo);
    }).catch((error) => {
      console.error(error);
    })
  }

  public getLineasApi = ( event: ojSelectSingle.valueChanged<string, Record<string, string>> ) =>{
    let idMarca =  parseInt(event.detail.value);
    getLinea().then((response: Array<ILinea>) => {
      let arreglo = []
      response.map((linea, index)=>{
        if(linea.id_marca == idMarca){
          arreglo.push({value:linea.id.toString(), label:linea.nombre})
        }
      })
      //console.log(arreglo)
      this.lineas(arreglo)
    }).catch((error) => {
      console.error(error);
    })
  }

  public lineaSelect = ( event: ojSelectSingle.valueChanged<string, Record<string, string>> ) =>{
    this.idlinea(parseInt(event.detail.value));
   }

  public getSubLineasApi = () => {
    get().then((response: Array<ISublinea>) => {
      this.sublineas(response)
    }).catch((error) => {
      console.error(error);
    })
  }



  guardarSubLinea = (event: ojButton.ojAction) => {
    const nuevaSublinea: ISublinea = {
      id: null,
      nombre: this.nombreSubLinea(),
      idlinea: this.idlinea()
    }
    create(nuevaSublinea).then((response: ISublinea) => {
      (document.getElementById("modal-crear") as ojDialog).close();
      this.getSubLineasApi()
      console.log(response)
    }).catch((error) => {
      console.error(error)
    })
  }


  public seleccionarCelda = (event: ojTable.selectedChanged<ISublinea['id'], ISublinea>) => {

    const row = event.detail.value.row as KeySetImpl<Number>;

    if (row.values().size > 0) {
      row.values().forEach((key) => {
        var selectedRow = this.sublineas().find(p => p.id === key);
        this.idLineaEditar(selectedRow.id_linea.toString());
        this.idMarcaEditar(selectedRow.id_marca.toString());
        this.nombreSubLineaeditar(selectedRow.nombre);
        this.idSelecion(selectedRow.id);
      })
    }

  }

  editarSubLinea = (event: ojButton.ojAction) => {
    const cambiosSublinea: ISublinea = {
      id: this.idSelecion(),
      nombre: this.nombreSubLineaeditar(),
      idlinea: this.idLineaEditar()

    }

    update(cambiosSublinea).then((response) => {
      //cerramos modal y traemos los productos

      this.getSubLineasApi();

      (document.getElementById("modal-editar") as ojDialog).close();

    }).catch((error) => {
      console.error(error);
    })

  }

  eliminarSubLinea = (event: ojButton.ojAction) => {
    del(this.idSelecion()).then((response) => {
      this.getSubLineasApi();
      (document.getElementById("modal-eliminar") as ojDialog).close();
    }).catch((error) => {
      console.error(error);
    })
  }

  //<Modales>
  //Crear
  public close(event: ojButton.ojAction) {
    (document.getElementById("modal-crear") as ojDialog).close();
  }

  public open(event: ojButton.ojAction) {
    (document.getElementById("modal-crear") as ojDialog).open();
  }

  //Editar
  public closeEditar(event: ojButton.ojAction) {
    (document.getElementById("modal-editar") as ojDialog).close();
  }

  public openEditar(event: ojButton.ojAction) {
    (document.getElementById("modal-editar") as ojDialog).open();
  }

  //Eliminar
  public closeEliminar(event: ojButton.ojAction) {
    (document.getElementById("modal-eliminar") as ojDialog).close();
  }

  public openEliminar(event: ojButton.ojAction) {
    (document.getElementById("modal-eliminar") as ojDialog).open();
  }
}

export = SubLineaViewModel;